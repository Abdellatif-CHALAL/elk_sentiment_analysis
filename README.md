# ELK_SENTIMENT_ANALYSIS

Analysis of feelings on the impact of the people on Marine Le Pen on the presidential elections of 2022 



- npm install 
- npm install twitter
- installer jq: sur
 - Mac : "brew install jq"
 - Linux : "apt-get install jq"

# Pour lancer le script, il faut exécuter ces commandes suivants pour générer le NDJson:
- node twitterApi.js
- cat group7.json | jq -c '.[]' > group7NDJSON.json 

# Pour poster les documents dans elaticsearch avec l'index group7:
- curl -s -H "Content-Type: application/x-ndjson" -XPOST localhost:9200/_bulk --data-binary @group7NDJSON.json